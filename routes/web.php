<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\AnggotaController;
use App\Http\Controllers\AuthorController;
use App\Http\Controllers\ContributorController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\PekerjaController;
use App\Http\Controllers\PesertaController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('home/index');
// });

Route::get('/', [HomeController::class, 'index'])->name('home');
Route::get('/datapekerja', [HomeController::class, 'datapekerja'])->name('datapekerja');


Auth::routes();

    Route::get('/profile', [AdminController::class, 'profile'])->name('adminprofile');

    // Peserta
    Route::get('/peserta', [PesertaController::class, 'index'])->name('peserta');
    Route::get('/peserta/template', [PesertaController::class, 'downloadtemplate'])->name('templatepeserta');
    Route::get('/peserta/tambah', [PesertaController::class, 'create'])->name('pesertacreate');
    Route::post('/peserta/store', [PesertaController::class, 'store'])->name('pesertastore');
    Route::get('/peserta/edit/{id}', [PesertaController::class, 'edit'])->name('pesertaedit');
    Route::post('/peserta/update/{id}', [PesertaController::class, 'update'])->name('pesertaupdate');
    Route::post('/peserta/import', [PesertaController::class, 'import_excel'])->name('pesertaimport');
    Route::get('/peserta/deleteall', [PesertaController::class, 'destroyall'])->name('pesertadeleteall');
    Route::get('/peserta/get-json', [PesertaController::class, 'PesertaJson'])->name('pesertajson');

    Route::get('/peserta/download', [PesertaController::class, 'downloadindex'])->name('pesertadownload');
    Route::get('/peserta/download/get-json', [PesertaController::class, 'WilayahJson'])->name('wilayahjson');
    Route::get('/peserta/download/print/{id}', [PesertaController::class, 'print'])->name('pesertaprint');
    Route::get('/peserta/download/get-json/{id}', [PesertaController::class, 'DownloadJson'])->name('downloadjson');

    Route::get('/peserta/{id}', [PesertaController::class, 'show'])->name('pesertashow');
    Route::delete('/peserta/{id}', [PesertaController::class, 'destroy'])->name('pesertadestroy');
    // End Peserta

    //Pekerja
    Route::get('/pekerja', [PekerjaController::class, 'index'])->name('pekerja');
    Route::get('/pekerja/template', [PekerjaController::class, 'downloadtemplate'])->name('templatepekerja');
    Route::get('/pekerja/tambah', [PekerjaController::class, 'create'])->name('pekerjacreate');
    Route::post('/pekerja/store', [PekerjaController::class, 'store'])->name('pekerjastore');
    Route::get('/pekerja/edit/{id}', [PekerjaController::class, 'edit'])->name('pekerjaedit');
    Route::post('/pekerja/update/{id}', [PekerjaController::class, 'update'])->name('pekerjaupdate');
    Route::post('/pekerja/import', [PekerjaController::class, 'import_excel'])->name('pekerjaimport');
    Route::get('/pekerja/destroyall', [PekerjaController::class, 'destroyall'])->name('pekerjadestroyall');
    Route::get('/pekerja/get-json', [PekerjaController::class, 'PekerjaJson'])->name('pekerjajson');

    Route::get('/pekerja/jamsostek/{id}', [PekerjaController::class, 'editjamsostek'])->name('editjamsostek');
    Route::post('/pekerja/updatejamsostek/{id}', [PekerjaController::class, 'updatejamsostek'])->name('updatejamsostek');

    Route::get('/pekerja/download', [PekerjaController::class, 'downloadindex'])->name('pekerjadownload');
    Route::get('/pekerja/download/get-json', [PekerjaController::class, 'WilayahJson'])->name('wilayahjsonpekerja');
    Route::get('/pekerja/download/print/{id}', [PekerjaController::class, 'print'])->name('pekerjaprint');
    Route::get('/pekerja/download/get-json/{id}', [PekerjaController::class, 'DownloadJson'])->name('pekerjadownloadjson');

    Route::get('/pekerja/{id}', [PekerjaController::class, 'show'])->name('pekerjashow');
    Route::delete('/pekerja/{id}', [PekerjaController::class, 'destroy'])->name('pekerjadestroy');

Route::group(['middleware' => ['auth']], function(){

    Route::group(['middleware' => ['login:1']], function(){

      Route::get('/dashboard', [AdminController::class, 'index'])->name('admindashboard');

      // Profile

      Route::get('/profile/edit', [AdminController::class, 'edit'])->name('adminedit');
      Route::post('/profile/update', [AdminController::class, 'update'])->name('adminupdate');
      Route::get('/profile/password', [AdminController::class, 'password'])->name('adminpassword');
      Route::post('/profile/password/update', [AdminController::class, 'updatepassword'])->name('adminpasswordupdate');
      // End Profile

      // Anggota
      Route::get('/anggota', [AnggotaController::class, 'index'])->name('anggota');
      Route::get('/anggota/tambah', [AnggotaController::class, 'create'])->name('anggotacreate');
      Route::post('/anggota/store', [AnggotaController::class, 'store'])->name('anggotastore');
      Route::get('/anggota/get-json', [AnggotaController::class, 'AnggotaJson'])->name('anggotajson');
      Route::post('/anggota/update/{id}', [AnggotaController::class, 'update'])->name('anggotaupdate');
      Route::get('/anggota/edit/{id}', [AnggotaController::class, 'edit'])->name('anggotaedit');
      Route::get('/anggota/{id}', [AnggotaController::class, 'show'])->name('anggotashow');
      Route::delete('/anggota/{id}', [AnggotaController::class, 'destroy'])->name('anggotadestroy');
      //End Anggota


      // End Pekerja
    });

    Route::group(['middleware' => ['login:2']], function(){
      Route::get('/contributor/dashboard', [ContributorController::class, 'index'])->name('contributordashboard');
      Route::get('/contributor/profile', [ContributorController::class, 'profile'])->name('contributorprofile');

    });


    Route::group(['middleware' => ['login:3']], function(){
      Route::get('/author/dashboard', [AuthorController::class, 'index'])->name('contributordashboard');
      Route::get('/author/profile', [AuthorController::class, 'profile'])->name('contributorprofile');

    });

  });
