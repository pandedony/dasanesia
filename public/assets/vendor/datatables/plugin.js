require.config({
    shim: {
        'datatables': ['jquery','core'],
    },
    paths: {
        'datatables': 'assets/vendor/datatables/datatables.min',
    }
});
