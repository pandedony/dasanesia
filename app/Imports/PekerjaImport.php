<?php

namespace App\Imports;

use App\Models\Pekerja;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Validators\Failure;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\SkipsOnError;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Concerns\SkipsErrors;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Concerns\WithBatchInserts;

class PekerjaImport implements ToModel, SkipsOnError, WithBatchInserts
{
  use Importable, SkipsErrors;
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
      $fields = [
        'field_one'     => $row[0]
      ];
      if (Validator::make($fields, [
          'field_one' => 'required|numeric',
      ])->fails()) {
          return null;
      }

      if (is_numeric($row['3'])){
        $tgl_lahir = intval($row['3']);
        $tgl_lahir_str = strtotime(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($tgl_lahir)->format('Y/m/d'));
        $tgl_lahir_hasil = date('Y-m-d H:i:s', $tgl_lahir_str);
      }
      else
      {
        $tgl_lahir_hasil = date('Y-m-d', strtotime($row['3']));
      }
        return new Pekerja([
          'nik' => $row[0],
          'nama' => $row[1],
          'jk' => $row[2],
          'tgl_lahir' => $tgl_lahir_hasil,
          'pendidikan' => $row[4],
          'pekerjaan' => $row[5],
          'alamat' => $row[6],
          'kabupaten' => $row[7],
      ]);
    }

    public function batchSize(): int
    {
        return 1000;
    }
}
