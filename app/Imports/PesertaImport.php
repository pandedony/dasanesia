<?php

namespace App\Imports;

use App\Models\Peserta;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Validators\Failure;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\SkipsOnError;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Concerns\SkipsErrors;
use Illuminate\Support\Facades\Validator;

class PesertaImport implements ToModel, SkipsOnError
{
  use Importable, SkipsErrors;
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
      $fields = [
        'field_one'     => $row[1]
      ];
      if (Validator::make($fields, [
          'field_one' => 'required|numeric',
      ])->fails()) {
          return null;
      }

      // $date1= $row['4']);
      if (is_numeric($row['4'])){
        $tgl_lahir = intval($row['4']);
        $tgl_lahir_str = strtotime(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($tgl_lahir)->format('Y/m/d'));
        $tgl_lahir_hasil = date('Y-m-d H:i:s', $tgl_lahir_str);
      }
      else
      {
        $tgl_lahir_hasil = date('Y-m-d', strtotime($row['4']));
      }

      if (is_numeric($row['6'])){
      $tgl_grace = intval($row['6']);
      $tgl_grace_str = strtotime(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($tgl_grace)->format('Y/m/d'));
      $tgl_grace_hasil = date('Y-m-d H:i:s', $tgl_grace_str);
      }
      else
      {
        $tgl_grace_hasil = date('Y-m-d', strtotime($row['6']));
      }

        return new Peserta([
          'nik' => $row[1],
          'nama' => $row[2],
          'kecamatan' => $row[3],
          'tgl_lahir' => $tgl_lahir_hasil,
          'program' => $row[5],
          'tgl_grace' => $tgl_grace_hasil,
          'kanal_daftar' => $row[7],
          'kanal_bayar' => $row[8],
          'nomor_hp' => $row[9],
      ]);
    }
}
