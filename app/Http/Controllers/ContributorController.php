<?php

namespace App\Http\Controllers;

use App\Models\Pekerja;
use App\Models\User;
use App\Models\Wilayah;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ContributorController extends Controller
{
    public function index()
    {
      $userid = Auth::user()->id;
      $user = User::where('id', $userid)->first();
      $totalpekerja = Wilayah::sum('total_pekerja');
      $totalpeserta = Wilayah::sum('total_peserta');
      $pekerja = Pekerja::take(10)
      ->latest()
      ->get();


      return view('contributor/dashboard/index', compact('user' , 'totalpekerja', 'totalpeserta', 'pekerja'))->render();
    }

    public function profile()
    {
      $userid = Auth::user()->id;
      $user = User::where('id', $userid)->first();

      return view('contributor/profile/index', compact('user'))->render();
    }
}
