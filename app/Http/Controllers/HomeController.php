<?php

namespace App\Http\Controllers;

use App\Models\Wilayah;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
      $totalpekerja = Wilayah::sum('total_pekerja');
      $totalpeserta = Wilayah::sum('total_peserta');
        return view('home/index', compact('totalpekerja', 'totalpeserta'))->render();
    }

    public function datapekerja()
    {
      $wilayah = Wilayah::All();
      return view('home/datapekerja', compact('wilayah'))->render();
    }
}
