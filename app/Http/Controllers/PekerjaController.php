<?php

namespace App\Http\Controllers;

use App\Imports\PekerjaImport;
use App\Models\Pekerja;
use App\Models\Peserta;
use App\Models\User;
use App\Models\Wilayah;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\Facades\DataTables;


class PekerjaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      if(!Auth::user())
      {
        return redirect('/');
      }
      $userid = Auth::user()->id;
      $user = User::where('id', $userid)->first();

      return view('administrator/pekerja/index', compact('user'))->render();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      if(Auth::user()->role != '1')
      {
        Auth::logout();
        return redirect('/login')->with('status', "Silahkan Login Terlebih Dahulu");
      }
      $userid = Auth::user()->id;
      $user = User::where('id', $userid)->first();
      $wilayah = Wilayah::All();

      return view('administrator/pekerja/create', compact('user', 'wilayah'))->render();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      if(Auth::user()->role != '1')
      {
        Auth::logout();
        return redirect('/login')->with('status', "Silahkan Login Terlebih Dahulu");
      }
      $wilayah= Wilayah::where('kode_wilayah', $request->kecamatan)->first();

      $request->validate([
        'nik' => 'required|numeric|digits:16',
        'nama' => 'required',
        'jk' => 'required',
        'tgl_lahir' => 'required',
        // 'pendidikan' => 'required',
        'pekerjaan' => 'required',
        // 'kelurahan' => 'required',
        // 'rt' => 'required',
        // 'rw' => 'required',
        'kecamatan' => 'required',
      ]);
      if(empty($request->rw))
      {
        $rw = '0';
      }
      else
      {
        $rw = $request->rw;
      }
      if(empty($request->rt))
      {
        $rt = '0';
      }
      else
      {
        $rt = $request->rt;
      }

      $alamat = $request->kelurahan.' RT '.$rt.' RW '. $rw. ' DUSUN KEC. ' . $wilayah->nama_wilayah . ' KAB. TIMOR TENGAH UTARA KODE POS 0';
      $caps = strtoupper($alamat);

      Pekerja::create([
        'nik' => $request->nik,
        'nama' => $request->nama,
        'jk' => $request->jk,
        'tgl_lahir' => $request->tgl_lahir,
        'pendidikan' => $request->pendidikan,
        'pekerjaan' => $request->pekerjaan,
        'alamat' => $caps,
        'kabupaten' => 'TIMOR TENGAH UTARA',
        'wilayah_id' => $request->kecamatan,
      ]);

      for ($i = 1; $i < 25; $i++) {
        $total = Pekerja::where('wilayah_id', 'NTT'.$i)->count();
        Wilayah::where('kode_wilayah', 'NTT'.$i)->update([
          'total_pekerja' => $total
        ]);
      }

      return redirect('/pekerja/')->with('success', 'Pekerja Berhasil Ditambahkan!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      if(!Auth::user())
      {
        return redirect('/');
      }
      $userid = Auth::user()->id;
      $user = User::where('id', $userid)->first();
      $pekerja = Pekerja::where('nik', $id)->first();
      $peserta = Peserta::where('nik',$id)->first();

      return view('administrator/pekerja/show', compact('user', 'pekerja', 'peserta'))->render();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      if(Auth::user()->role != '1')
      {
        Auth::logout();
        return redirect('/login')->with('status', "Silahkan Login Terlebih Dahulu");
      }
      $userid = Auth::user()->id;
      $user = User::where('id', $userid)->first();
      $pekerja = pekerja::where('nik', $id)->first();
      $wilayah = Wilayah::All();
      $userwilayah = Wilayah::where('kode_wilayah', $pekerja->wilayah_id)->first();

      return view('administrator/pekerja/edit', compact('user', 'pekerja', 'wilayah', 'userwilayah'))->render();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      if(Auth::user()->role != '1')
      {
        Auth::logout();
        return redirect('/login')->with('status', "Silahkan Login Terlebih Dahulu");
      }
      $wilayah = Wilayah::where('kode_wilayah', $request->kecamatan)->first();
      $request->validate([
        'nik' => 'required|numeric|digits:16',
        'nama' => 'required',
        'jk' => 'required',
        'tgl_lahir' => 'required',
        'pendidikan' => 'required',
        'pekerjaan' => 'required',
        'alamat' => 'required',
        'kecamatan' => 'required',
      ]);

      Pekerja::where('nik', $id)->update([
        'nik' => $request->nik,
        'nama' => $request->nama,
        'jk' => $request->jk,
        'tgl_lahir' => $request->tgl_lahir,
        'pendidikan' => $request->pendidikan,
        'pekerjaan' => $request->pekerjaan,
        'alamat' => $request->alamat,
        'kabupaten' => 'TIMOR TENGAH UTARA',
        'wilayah_id' => $request->kecamatan,
      ]);

      for ($i = 1; $i < 25; $i++) {
        $total = Pekerja::where('wilayah_id', 'NTT'.$i)->count();
        Wilayah::where('kode_wilayah', 'NTT'.$i)->update([
          'total_pekerja' => $total
        ]);
      }

      return redirect('/pekerja/'.$id)->with('success', 'Pekerja Berhasil Di Ubah!');
    }


    public function editjamsostek($id)
    {
      if(Auth::user()->role != '1')
      {
        Auth::logout();
        return redirect('/login')->with('status', "Silahkan Login Terlebih Dahulu");
      }
      $userid = Auth::user()->id;
      $user = User::where('id', $userid)->first();
      $pekerja = Pekerja::where('nik', $id)->first();
      $wilayah = Wilayah::where('kode_wilayah', $pekerja->wilayah_id)->first();

      return view('administrator/pekerja/jamsostek/edit', compact('user', 'pekerja', 'wilayah'))->render();
    }

    public function updatejamsostek(Request $request, $id)
    {
      if(Auth::user()->role != '1')
      {
        Auth::logout();
        return redirect('/login')->with('status', "Silahkan Login Terlebih Dahulu");
      }
      $pekerja = Pekerja::where('nik', $id)->first();
      $wilayah = Wilayah::where('kode_wilayah', $pekerja->wilayah_id)->first();

      $request->validate([
        'program' => 'required',
        'tgl_grace' => 'required',
        'kanal_daftar' => 'required',
        'kanal_bayar' => 'required',
        'no_hp' => 'required|numeric',
      ]);

      Peserta::create([
        'nik' => $pekerja->nik,
        'nama' => $pekerja->nama,
        'kecamatan' => $wilayah->nama_wilayah,
        'tgl_lahir' => $pekerja->tgl_lahir,
        'program' => $request->program,
        'tgl_grace' => $request->tgl_grace,
        'kanal_daftar' => $request->kanal_daftar,
        'kanal_bayar' => $request->kanal_bayar,
        'nomor_hp' => $request->no_hp,
        'wilayah_id' => $pekerja->wilayah_id,
      ]);

      for ($i = 1; $i < 25; $i++) {
        $total = Peserta::where('wilayah_id', 'NTT'.$i)->count();
        Wilayah::where('kode_wilayah', 'NTT'.$i)->update([
          'total_peserta' => $total
        ]);
      }
      return redirect('/peserta/'.$pekerja->nik)->with('success', 'Peserta Berhasil Ditambahkan!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      if(Auth::user()->role != '1')
      {
        Auth::logout();
        return redirect('/login')->with('status', "Silahkan Login Terlebih Dahulu");
      }
      if(!empty(Peserta::where('nik', $id)->first()))
      {
        return redirect('pekerja')->with('success', 'Data Tidak Terhapus, Karena Masih Peserta');
      }
      Pekerja::where('nik', $id)->delete();
      for ($i = 1; $i < 25; $i++) {
        $total = Pekerja::where('wilayah_id', 'NTT'.$i)->count();
        Wilayah::where('kode_wilayah', 'NTT'.$i)->update([
          'total_pekerja' => $total
        ]);
      }

      return redirect('pekerja')->with('success', 'Data Pekerja berhasil dihapus.');
    }

    public function destroyall()
    {
      if(Auth::user()->role != '1')
      {
        Auth::logout();
        return redirect('/login')->with('status', "Silahkan Login Terlebih Dahulu");
      }
      if(empty(Pekerja::count())){
        return redirect('/pekerja/')->with('success', 'Data Masih Kosong!');
      }
      Pekerja::query()->delete();
      return redirect('/pekerja/')->with('success', 'Data Pekerja berhasil dihapus!');
    }



    public function import_excel(Request $request)
    {
      if(!Auth::user())
      {
        return redirect('/');
      }
      $this->validate($request, [
        'file' => 'required|mimes:csv,xls,xlsx'
      ]);

      // menangkap file excel
      $file = $request->file('file');

      // membuat nama file unik
      $nama_file = rand().$file->getClientOriginalName();

      // upload ke folder file_siswa di dalam folder public
      $file->move('assets/excel/',$nama_file);

      // import data
      Excel::import(new PekerjaImport, public_path('/assets/excel/'.$nama_file));

      Pekerja::where('alamat', 'like', "%". 'MIOMAFO' ."%")->update([
        "alamat" => Pekerja::raw("REPLACE(alamat,  'MIOMAFO', 'MIOMAFFO')")
      ]);

      // Update Lokasi
      for ($i = 1; $i < 25; $i++) {
        $wilayah = Wilayah::where('kode_wilayah', 'NTT'.$i)->first();
        Pekerja::where('alamat', 'like', "%". $wilayah->nama_wilayah ."%")->where('wilayah_id', null)->update([
          'wilayah_id' => 'NTT'.$i
        ]);
      }

      for ($i = 1; $i < 25; $i++) {
        $total = Pekerja::where('wilayah_id', 'NTT'.$i)->count();
        Wilayah::where('kode_wilayah', 'NTT'.$i)->update([
          'total_pekerja' => $total
        ]);
      }

      // notifikasi dengan session
      Session::flash('sukses','Data Pekerja Berhasil Diimport!');

      // alihkan halaman kembali
      return redirect('/pekerja');
    }

    public function PekerjaJson()
    {
      if(!Auth::user())
      {
        return redirect('/');
      }
      // $peserta = Peserta::All();
      return DataTables::of(Pekerja::query())
      ->addIndexColumn()
      ->addColumn('action', function($pekerja) {
        return view('administrator.pekerja.datatables.action',compact('pekerja'))->render();
      })
      ->addColumn('tanggallahir', function($pekerja) {
        return $pekerja->tgl_lahir->format('d F Y');
      })
      ->make(true);
    }

    public function downloadindex()
    {
      if(!Auth::user())
      {
        return redirect('/');
      }
      $userid = Auth::user()->id;
      $user = User::where('id', $userid)->first();
      return view('administrator/pekerja/download/index', compact('user'))->render();
    }

    public function WilayahJson()
    {
      if(!Auth::user())
      {
        return redirect('/');
      }
      // $peserta = Peserta::All();
      return DataTables::of(Wilayah::query())
      ->addIndexColumn()
      ->addColumn('action', function($wilayah) {
        return view('administrator.pekerja.download.action',compact('wilayah'))->render();
      })
      ->addColumn('jumlah', function($wilayah) {
        return number_format($wilayah->total_pekerja);
      })
      ->make(true);
    }

    public function print($id)
    {
      if(!Auth::user())
      {
        return redirect('/');
      }
      $userid = Auth::user()->id;
      $user = User::where('id', $userid)->first();
      $wilayah = Wilayah::where('kode_wilayah', $id)->first();

      return view('administrator/pekerja/download/print', compact('user', 'wilayah'))->render();
    }

    public function DownloadJson($id)
    {
      if(!Auth::user())
      {
        return redirect('/');
      }
      // $peserta = Peserta::All();
      return DataTables::of(Pekerja::where('wilayah_id',$id)->get())
      ->addIndexColumn()
      ->addColumn('tanggallahir', function($pekerja) {
        return $pekerja->tgl_lahir->format('d F Y');
      })
      ->make(true);
    }

    public function downloadtemplate()
    {
      if(!Auth::user())
      {
        return redirect('/');
      }
      $filePath = public_path("assets/excel/pekerja/templatepekerja.xlsx");
    	$headers = ['Content-Type: application/vnd.ms-excel'];
    	$fileName = 'Template Pekerja.xlsx';

    	return response()->download($filePath, $fileName, $headers);
    }

}
