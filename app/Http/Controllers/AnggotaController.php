<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Facades\DataTables;


class AnggotaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $userid = Auth::user()->id;
      $user = User::where('id', $userid)->first();

      return view('administrator/anggota/index', compact('user'))->render();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $userid = Auth::user()->id;
      $user = User::where('id', $userid)->first();

      return view('administrator/anggota/create', compact('user'))->render();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $request->validate([
        'name' => 'required',
        'username' => 'required|regex:/^\S*$/u|min:6|unique:users',
        'password' => 'regex:/^[^\s]+(\s*[^\s]+)*$/|min:8|confirmed|required',
        'role' => 'required|numeric',
      ]);

      User::create([
        'name' => $request->name,
        'username' => $request->username,
        'password' => bcrypt($request->password),
        'role' => $request->role,
      ]);

      return redirect('/anggota/')->with('success', 'Anggota Berhasil Ditambahkan!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $userid = Auth::user()->id;
      $user = User::where('id', $userid)->first();
      $anggota = User::where('id', $id)->first();

      return view('administrator/anggota/show', compact('user', 'anggota'))->render();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $userid = Auth::user()->id;
      $user = User::where('id', $userid)->first();
      $anggota = User::where('id', $id)->first();

      return view('administrator/anggota/edit', compact('user', 'anggota'))->render();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $user = User::where('id', $id)->first();

      if($request->username != $user->username)
      {
        $request->validate([
          'username' => 'required|regex:/^\S*$/u|min:6|unique:users',
        ]);
      }

      if(empty($request->password))
      {
        $request->validate([
          'name' => 'required',
          'role' => 'required|numeric',
        ]);

        User::where('id', $id)->update([
          'name' => $request->name,
          'username' => $request->username,
          'role' => $request->role,
        ]);
      }
      else
      {
        $request->validate([
          'name' => 'required',
          'password' => 'regex:/^[^\s]+(\s*[^\s]+)*$/|min:8|confirmed|required',
          'role' => 'required|numeric',
        ]);

        User::where('id', $id)->update([
          'name' => $request->name,
          'username' => $request->username,
          'password' => bcrypt($request->password),
          'role' => $request->role,
        ]);
      }

      return redirect('anggota/'.$id)->with('success', 'Data Anggota berhasil diupdate.');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $anggota = User::findOrFail($id);
      $anggota->delete();

      return redirect('anggota')->with('success', 'Data Anggota berhasil dihapus.');
    }

    public function AnggotaJson()
    {
      $anggota = User::orderBy('id', 'desc')->where('role','!=','1')->get();
      return DataTables::of($anggota)
      ->addIndexColumn()
      ->addColumn('action', function($anggota) {
        return view('administrator.anggota.datatables.action',compact('anggota'))->render();
      })
      ->addColumn('tanggal', function($anggota) {
        return $anggota->created_at->format('d F Y');
      })
      ->addColumn('role', function($anggota) {
        if($anggota -> role == '1')
        {
          return 'Administrator';
        }
        elseif($anggota -> role == '2')
        {
          return 'Contributor';
        }
        elseif($anggota -> role == '3')
        {
          return 'Author';
        }
    })
      ->toJson();
    }
}
