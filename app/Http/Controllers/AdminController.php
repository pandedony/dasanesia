<?php

namespace App\Http\Controllers;

use App\Models\Pekerja;
use App\Models\User;
use App\Models\Wilayah;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Rules\CurrentPassword;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
      if(!Auth::user())
      {
        return redirect('/');
      }
      $userid = Auth::user()->id;
      $user = User::where('id', $userid)->first();
      $totalpekerja = Wilayah::sum('total_pekerja');
      $totalpeserta = Wilayah::sum('total_peserta');
      $pekerja = Pekerja::take(10)
      ->latest()
      ->get();


      return view('administrator/dashboard/index', compact('user' , 'totalpekerja', 'totalpeserta', 'pekerja'))->render();
    }

    public function profile()
    {
      $userid = Auth::user()->id;
      $user = User::where('id', $userid)->first();

      return view('administrator/profile/index', compact('user'))->render();
    }

    public function edit()
    {
      $userid = Auth::user()->id;
      $user = User::where('id', $userid)->first();

      return view('administrator/profile/edit', compact('user'))->render();
    }

    public function update(Request $request)
    {
      $userid = Auth::user()->id;
      $user = User::where('id', $userid)->first();

      if($request->username == $user->username)
      {
        $request->validate([
          'name' => 'required',
        ]);
      }
      else
      {
        $request->validate([
          'name' => 'required',
          'username' => 'required|regex:/^\S*$/u|min:6|unique:users',
        ]);
      }


      User::where('id', $user->id)->update([
        'name' => $request->name,
        'username' => $request->username,
      ]);

      return redirect('profile')->with('success', 'Data Berhasil Diupdate.');
    }

    public function password()
    {
      $userid = Auth::user()->id;
      $user = User::where('id', $userid)->first();

      return view('administrator/profile/password', compact('user'))->render();
    }

    public function updatepassword(Request $request)
    {
      $userid = Auth::user()->id;
      $user = User::where('id', $userid)->first();

      // dd($request->current_password);

      $request->validate([
        'current_password' => ['required', new CurrentPassword],
        'new_password' => ['regex:/^[^\s]+(\s*[^\s]+)*$/|min:8|confirmed|required'],
      ]);

      User::where('id', $user->id)->update([
        'password' => bcrypt($request->password),
      ]);
      return redirect('profile')->with('success', 'Data Berhasil Diupdate.');
    }
}
