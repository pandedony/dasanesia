<?php

namespace App\Http\Controllers;

use App\Imports\PesertaImport;
use App\Models\Peserta;
use App\Models\User;
use App\Models\Wilayah;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Session;

class PesertaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      if(!Auth::user())
      {
        return redirect('/');
      }
      $userid = Auth::user()->id;
      $user = User::where('id', $userid)->first();

      return view('administrator/peserta/index', compact('user'))->render();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      if(Auth::user()->role == '1' || Auth::user()->role == '2')
      {

        $userid = Auth::user()->id;
        $user = User::where('id', $userid)->first();
        $wilayah = Wilayah::All();

        return view('administrator/peserta/create', compact('user', 'wilayah'))->render();
      }
      Auth::logout();
      return redirect('/login')->with('status', "Silahkan Login Terlebih Dahulu");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      if(Auth::user()->role == '1' || Auth::user()->role == '2')
      {
        $wilayah= Wilayah::where('kode_wilayah', $request->kecamatan)->first();

        $request->validate([
          'nik' => 'required|numeric|digits:16',
          'nama' => 'required',
          'tgl_lahir' => 'required',
          'kecamatan' => 'required',
          'program' => 'required',
          'tgl_grace' => 'required',
          'kanal_daftar' => 'required',
          'kanal_bayar' => 'required',
          'no_hp' => 'required|numeric',
        ]);

        Peserta::create([
          'nik' => $request->nik,
          'nama' => $request->nama,
          'kecamatan' => $wilayah->nama_wilayah,
          'tgl_lahir' => $request->tgl_lahir,
          'program' => $request->program,
          'tgl_grace' => $request->tgl_grace,
          'kanal_daftar' => $request->kanal_daftar,
          'kanal_bayar' => $request->kanal_bayar,
          'nomor_hp' => $request->no_hp,
          'wilayah_id' => $request->kecamatan,
        ]);

        for ($i = 1; $i < 25; $i++) {
          $total = Peserta::where('wilayah_id', 'NTT'.$i)->count();
          Wilayah::where('kode_wilayah', 'NTT'.$i)->update([
            'total_peserta' => $total
          ]);
        }

        return redirect('/peserta/')->with('success', 'Peserta Berhasil Ditambahkan!');
      }
      Auth::logout();
      return redirect('/login')->with('status', "Silahkan Login Terlebih Dahulu");

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      if(!Auth::user())
      {
        return redirect('/');
      }
      $userid = Auth::user()->id;
      $user = User::where('id', $userid)->first();
      $peserta = Peserta::where('nik', $id)->first();

      return view('administrator/peserta/show', compact('user', 'peserta'))->render();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      if(Auth::user()->role != '1')
      {
        Auth::logout();
        return redirect('/login')->with('status', "Silahkan Login Terlebih Dahulu");
      }
      $userid = Auth::user()->id;
      $user = User::where('id', $userid)->first();
      $peserta = Peserta::where('nik', $id)->first();
      $wilayah = Wilayah::All();

      return view('administrator/peserta/edit', compact('user', 'peserta', 'wilayah'))->render();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      if(Auth::user()->role != '1')
      {
        Auth::logout();
        return redirect('/login')->with('status', "Silahkan Login Terlebih Dahulu");
      }
      $wilayah = Wilayah::where('kode_wilayah', $request->kecamatan)->first();
      $request->validate([
        'nik' => 'required|numeric|digits:16',
        'nama' => 'required',
        'tgl_lahir' => 'required',
        'kecamatan' => 'required',
        'program' => 'required',
        'tgl_grace' => 'required',
        'kanal_daftar' => 'required',
        'kanal_bayar' => 'required',
        'no_hp' => 'required|numeric',
      ]);

      Peserta::where('nik', $id)->update([
        'nik' => $request->nik,
        'nama' => $request->nama,
        'kecamatan' => $wilayah->nama_wilayah,
        'tgl_lahir' => $request->tgl_lahir,
        'program' => $request->program,
        'tgl_grace' => $request->tgl_grace,
        'kanal_daftar' => $request->kanal_daftar,
        'kanal_bayar' => $request->kanal_bayar,
        'nomor_hp' => $request->no_hp,
        'wilayah_id' => $request->kecamatan,
      ]);

      for ($i = 1; $i < 25; $i++) {
        $total = Peserta::where('wilayah_id', 'NTT'.$i)->count();
        Wilayah::where('kode_wilayah', 'NTT'.$i)->update([
          'total_peserta' => $total
        ]);
      }

      return redirect('/peserta/'.$id)->with('success', 'Peserta Berhasil Di Ubah!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      if(Auth::user()->role != '1')
      {
        Auth::logout();
        return redirect('/login')->with('status', "Silahkan Login Terlebih Dahulu");
      }
      Peserta::where('nik', $id)->delete();
      for ($i = 1; $i < 25; $i++) {
        $total = Peserta::where('wilayah_id', 'NTT'.$i)->count();
        Wilayah::where('kode_wilayah', 'NTT'.$i)->update([
          'total_peserta' => $total
        ]);
      }

      return redirect('peserta')->with('success', 'Data Anggota berhasil dihapus.');
    }

    public function destroyall()
    {
      if(Auth::user()->role != '1')
      {
        Auth::logout();
        return redirect('/login')->with('status', "Silahkan Login Terlebih Dahulu");
      }
      if(empty(Peserta::count())){
        return redirect('/peserta/')->with('success', 'Data Masih Kosong!');
      }
      Peserta::query()->delete();

      for ($i = 1; $i < 25; $i++) {
        $total = Peserta::where('wilayah_id', 'NTT'.$i)->count();
        Wilayah::where('kode_wilayah', 'NTT'.$i)->update([
          'total_peserta' => $total
        ]);
      }
      return redirect('/peserta/')->with('success', 'Data Peserta berhasil dihapus!');

    }

    public function import_excel(Request $request)
    {
      if(!Auth::user())
      {
        return redirect('/');
      }
      $this->validate($request, [
        'file' => 'required|mimes:csv,xls,xlsx'
      ]);

      // menangkap file excel
      $file = $request->file('file');

      // membuat nama file unik
      $nama_file = rand().$file->getClientOriginalName();

      // upload ke folder file_siswa di dalam folder public
      $file->move('assets/excel/',$nama_file);

      // import data
      Excel::import(new PesertaImport, public_path('/assets/excel/'.$nama_file));

      Peserta::where('kecamatan', 'like', "%". 'MIOMAFO' ."%")->update([
        "kecamatan" => Peserta::raw("REPLACE(kecamatan,  'MIOMAFO', 'MIOMAFFO')")
      ]);

      // Update Lokasi
      for ($i = 1; $i < 25; $i++) {
        $wilayah = Wilayah::where('kode_wilayah', 'NTT'.$i)->first();
        Peserta::where('kecamatan', 'like', "%". $wilayah->nama_wilayah ."%")->where('wilayah_id', null)->update([
          'wilayah_id' => 'NTT'.$i
        ]);
      }

      for ($i = 1; $i < 25; $i++) {
        $total = Peserta::where('wilayah_id', 'NTT'.$i)->count();
        Wilayah::where('kode_wilayah', 'NTT'.$i)->update([
          'total_peserta' => $total
        ]);
      }



      // notifikasi dengan session
      Session::flash('sukses','Data Peserta Berhasil Diimport!');

      // alihkan halaman kembali
      return redirect('/peserta');
    }

    public function PesertaJson()
    {
      if(!Auth::user())
      {
        return redirect('/');
      }
      // $peserta = Peserta::All();
      return DataTables::of(Peserta::query())
      ->addIndexColumn()
      ->addColumn('action', function($peserta) {
        return view('administrator.peserta.datatables.action',compact('peserta'))->render();
      })
      ->addColumn('tanggallahir', function($peserta) {
        return $peserta->tgl_lahir->format('d F Y');
      })
      ->addColumn('tanggalgrace', function($peserta) {
        return $peserta->tgl_grace->format('d F Y');
      })
      ->make(true);
    }

    public function downloadindex()
    {
      $userid = Auth::user()->id;
      $user = User::where('id', $userid)->first();
      return view('administrator/peserta/download/index', compact('user'))->render();
    }

    public function WilayahJson()
    {
      if(!Auth::user())
      {
        return redirect('/');
      }
      // $peserta = Peserta::All();
      return DataTables::of(Wilayah::query())
      ->addIndexColumn()
      ->addColumn('action', function($wilayah) {
        return view('administrator.peserta.download.action',compact('wilayah'))->render();
      })
      ->addColumn('jumlah', function($wilayah) {
        return number_format($wilayah->total_peserta);
      })
      ->make(true);
    }

    public function print($id)
    {
      if(!Auth::user())
      {
        return redirect('/');
      }
      $userid = Auth::user()->id;
      $user = User::where('id', $userid)->first();
      $wilayah = Wilayah::where('kode_wilayah', $id)->first();

      return view('administrator/peserta/download/print', compact('user', 'wilayah'))->render();
    }

    public function DownloadJson($id)
    {
      if(!Auth::user())
      {
        return redirect('/');
      }
      // $peserta = Peserta::All();
      return DataTables::of(Peserta::where('wilayah_id',$id)->get())
      ->addIndexColumn()
      ->addColumn('tanggallahir', function($peserta) {
        return $peserta->tgl_lahir->format('d F Y');
      })
      ->addColumn('tanggalgrace', function($peserta) {
        return $peserta->tgl_grace->format('d F Y');
      })
      ->make(true);
    }

    public function downloadtemplate()
    {
      if(!Auth::user())
      {
        return redirect('/');
      }
      $filePath = public_path("assets/excel/peserta/templatepeserta.xlsx");
    	$headers = ['Content-Type: application/vnd.ms-excel'];
    	$fileName = 'Template Peserta.xlsx';

    	return response()->download($filePath, $fileName, $headers);
    }

}
