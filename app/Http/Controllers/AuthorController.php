<?php

namespace App\Http\Controllers;

use App\Models\Pekerja;
use App\Models\User;
use App\Models\Wilayah;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthorController extends Controller
{
  public function index()
  {
    $userid = Auth::user()->id;
    $user = User::where('id', $userid)->first();
    $totalpekerja = Wilayah::sum('total_pekerja');
    $totalpeserta = Wilayah::sum('total_peserta');
    $pekerja = Pekerja::take(10)
    ->latest()
    ->get();


    return view('author/dashboard/index', compact('user' , 'totalpekerja', 'totalpeserta', 'pekerja'))->render();
  }

}
