<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Peserta extends Model
{
    use HasFactory;

    protected $table = 'peserta';
    protected $dates = ['tgl_lahir', 'tgl_grace'];

    protected $fillable = [
      'nik',
      'nama',
      'kecamatan',
      'tgl_lahir',
      'program',
      'tgl_grace',
      'kanal_daftar',
      'kanal_bayar',
      'nomor_hp',
      'wilayah_id',
    ];
}
