<?php

namespace Database\Seeders;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Administrator',
            'username' => 'admin@dasanesia.com',
            'password' => bcrypt('12344321'),
            'role' => '1',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
          ]);
    }
}
