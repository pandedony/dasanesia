<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class WilayahSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('wilayah')->insert([
        'kode_wilayah' => 'NTT1',
        'nama_wilayah' => 'BIBOKI ANLEU',
        'total_peserta' => null,
        'total_pekerja' => null,
        'created_at' => Carbon::now(),
        'updated_at' => Carbon::now()
      ]);

      DB::table('wilayah')->insert([
        'kode_wilayah' => 'NTT2',
        'nama_wilayah' => 'BIBOKI FEOTLEU',
        'total_peserta' => null,
        'total_pekerja' => null,
        'created_at' => Carbon::now(),
        'updated_at' => Carbon::now()
      ]);

      DB::table('wilayah')->insert([
        'kode_wilayah' => 'NTT3',
        'nama_wilayah' => 'BIBOKI MOENLEU',
        'total_peserta' => null,
        'total_pekerja' => null,
        'created_at' => Carbon::now(),
        'updated_at' => Carbon::now()
      ]);

      DB::table('wilayah')->insert([
        'kode_wilayah' => 'NTT4',
        'nama_wilayah' => 'BIBOKI SELATAN',
        'total_peserta' => null,
        'total_pekerja' => null,
        'created_at' => Carbon::now(),
        'updated_at' => Carbon::now()
      ]);

      DB::table('wilayah')->insert([
        'kode_wilayah' => 'NTT5',
        'nama_wilayah' => 'BIBOKI TAN PAH',
        'total_peserta' => null,
        'total_pekerja' => null,
        'created_at' => Carbon::now(),
        'updated_at' => Carbon::now()
      ]);

      DB::table('wilayah')->insert([
        'kode_wilayah' => 'NTT6',
        'nama_wilayah' => 'BIBOKI UTARA',
        'total_peserta' => null,
        'total_pekerja' => null,
        'created_at' => Carbon::now(),
        'updated_at' => Carbon::now()
      ]);

      DB::table('wilayah')->insert([
        'kode_wilayah' => 'NTT7',
        'nama_wilayah' => 'BIKOMI NILULAT',
        'total_peserta' => null,
        'total_pekerja' => null,
        'created_at' => Carbon::now(),
        'updated_at' => Carbon::now()
      ]);

      DB::table('wilayah')->insert([
        'kode_wilayah' => 'NTT8',
        'nama_wilayah' => 'BIKOMI SELATAN',
        'total_peserta' => null,
        'total_pekerja' => null,
        'created_at' => Carbon::now(),
        'updated_at' => Carbon::now()
      ]);

      DB::table('wilayah')->insert([
        'kode_wilayah' => 'NTT9',
        'nama_wilayah' => 'BIKOMI TENGAH',
        'total_peserta' => null,
        'total_pekerja' => null,
        'created_at' => Carbon::now(),
        'updated_at' => Carbon::now()
      ]);

      DB::table('wilayah')->insert([
        'kode_wilayah' => 'NTT10',
        'nama_wilayah' => 'BIKOMI UTARA',
        'total_peserta' => null,
        'total_pekerja' => null,
        'created_at' => Carbon::now(),
        'updated_at' => Carbon::now()
      ]);

      DB::table('wilayah')->insert([
        'kode_wilayah' => 'NTT11',
        'nama_wilayah' => 'INSANA BARAT',
        'total_peserta' => null,
        'total_pekerja' => null,
        'created_at' => Carbon::now(),
        'updated_at' => Carbon::now()
      ]);

      DB::table('wilayah')->insert([
        'kode_wilayah' => 'NTT12',
        'nama_wilayah' => 'INSANA FAFINESU',
        'total_peserta' => null,
        'total_pekerja' => null,
        'created_at' => Carbon::now(),
        'updated_at' => Carbon::now()
      ]);

      DB::table('wilayah')->insert([
        'kode_wilayah' => 'NTT13',
        'nama_wilayah' => 'INSANA TENGAH',
        'total_peserta' => null,
        'total_pekerja' => null,
        'created_at' => Carbon::now(),
        'updated_at' => Carbon::now()
      ]);

      DB::table('wilayah')->insert([
        'kode_wilayah' => 'NTT14',
        'nama_wilayah' => 'INSANA UTARA',
        'total_peserta' => null,
        'total_pekerja' => null,
        'created_at' => Carbon::now(),
        'updated_at' => Carbon::now()
      ]);

      DB::table('wilayah')->insert([
        'kode_wilayah' => 'NTT15',
        'nama_wilayah' => 'INSANA',
        'total_peserta' => null,
        'total_pekerja' => null,
        'created_at' => Carbon::now(),
        'updated_at' => Carbon::now()
      ]);

      DB::table('wilayah')->insert([
        'kode_wilayah' => 'NTT16',
        'nama_wilayah' => 'KOTA KEFAMENANU',
        'total_peserta' => null,
        'total_pekerja' => null,
        'created_at' => Carbon::now(),
        'updated_at' => Carbon::now()
      ]);

      DB::table('wilayah')->insert([
        'kode_wilayah' => 'NTT17',
        'nama_wilayah' => 'MIOMAFFO BARAT',
        'total_peserta' => null,
        'total_pekerja' => null,
        'created_at' => Carbon::now(),
        'updated_at' => Carbon::now()
      ]);

      DB::table('wilayah')->insert([
        'kode_wilayah' => 'NTT18',
        'nama_wilayah' => 'MIOMAFFO TENGAH',
        'total_peserta' => null,
        'total_pekerja' => null,
        'created_at' => Carbon::now(),
        'updated_at' => Carbon::now()
      ]);

      DB::table('wilayah')->insert([
        'kode_wilayah' => 'NTT19',
        'nama_wilayah' => 'MIOMAFFO TIMUR',
        'total_peserta' => null,
        'total_pekerja' => null,
        'created_at' => Carbon::now(),
        'updated_at' => Carbon::now()
      ]);

      DB::table('wilayah')->insert([
        'kode_wilayah' => 'NTT20',
        'nama_wilayah' => 'MUSI',
        'total_peserta' => null,
        'total_pekerja' => null,
        'created_at' => Carbon::now(),
        'updated_at' => Carbon::now()
      ]);

      DB::table('wilayah')->insert([
        'kode_wilayah' => 'NTT21',
        'nama_wilayah' => 'MUTIS',
        'total_peserta' => null,
        'total_pekerja' => null,
        'created_at' => Carbon::now(),
        'updated_at' => Carbon::now()
      ]);

      DB::table('wilayah')->insert([
        'kode_wilayah' => 'NTT22',
        'nama_wilayah' => 'NAIBENU',
        'total_peserta' => null,
        'total_pekerja' => null,
        'created_at' => Carbon::now(),
        'updated_at' => Carbon::now()
      ]);

      DB::table('wilayah')->insert([
        'kode_wilayah' => 'NTT23',
        'nama_wilayah' => 'NOEMUTI TIMUR',
        'total_peserta' => null,
        'total_pekerja' => null,
        'created_at' => Carbon::now(),
        'updated_at' => Carbon::now()
      ]);

      DB::table('wilayah')->insert([
        'kode_wilayah' => 'NTT24',
        'nama_wilayah' => 'NOEMUTI',
        'total_peserta' => null,
        'total_pekerja' => null,
        'created_at' => Carbon::now(),
        'updated_at' => Carbon::now()
      ]);
    }
}
