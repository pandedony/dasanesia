<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePesertaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('peserta', function (Blueprint $table) {
            $table->bigInteger('nik')->unique();
            $table->string('nama');
            $table->string('kecamatan');
            $table->date('tgl_lahir');
            $table->string('program');
            $table->date('tgl_grace');
            $table->string('kanal_daftar');
            $table->string('kanal_bayar');
            $table->string('nomor_hp', 20);
            $table->string('wilayah_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('peserta');
    }
}
