<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePekerjaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pekerja', function (Blueprint $table) {
          $table->bigInteger('nik')->unique();
          $table->string('nama');
          $table->string('jk');
          $table->date('tgl_lahir');
          $table->string('pendidikan')->nullable();
          $table->string('pekerjaan');
          $table->string('alamat');
          $table->string('kabupaten');
          $table->string('wilayah_id')->nullable();
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pekerja');
    }
}
