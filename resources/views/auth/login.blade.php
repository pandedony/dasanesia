@extends('layouts/homelayout')
@section('content')
<section id="services">
  <div class="container mt--6">
    <div class="row">
      <div class="col-md-12">
        <h3 class="title text-center">Login</h3>
        <div class="titleHR"><span></span></div>
      </div>
    </div>
    <form method="POST" action="{{ route('login') }}">
      @csrf

      <div class="form-group row">
          <label for="username" class="col-md-4 col-form-label text-md-right">{{ __('Username') }}</label>

          <div class="col-md-6">
              <input id="username" type="text" class="form-control @error('username') is-invalid @enderror" name="username" value="{{ old('username') }}" required autocomplete="username" autofocus>

              @error('username')
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                  </span>
              @enderror
          </div>
      </div>

      <div class="form-group row">
          <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

          <div class="col-md-6">
              <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

              @error('password')
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                  </span>
              @enderror
          </div>
      </div>


      <div class="form-group row mb-0">
          <div class="col-md-8 offset-md-4">
              <button type="submit" class="btn btn-primary">
                  {{ __('Login') }}
              </button>

          </div>
      </div>
    </form>

  </div> <!--/.container -->
</section>
<!-- / End services-->


@endsection
