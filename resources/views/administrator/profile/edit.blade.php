@extends('layouts/userlayout')

@section('title', 'Dasanesia - Profile')


@section('style')

@endsection

@section('mywidget')
  @include('administrator/dashboard/widget')
@endsection

@section('topnav')
  @include('administrator/dashboard/topnav')
@endsection


@section('content')

    <!-- Header -->
    <!-- Header -->
    <div class="header bg-primary pb-9">

    </div>

<div class="container-fluid mt--8">

  <div class="row">
    <div class="col-lg-8">
      <div class="card">
        <div class="card-header border-0">
          <div class="row align-items-center">
            <div class="col">
              <h2 class="mb-0">Edit Profile</h2>
            </div>
            <div class="col text-right">
              <a href="{{route('adminpassword')}}" class="btn btn-sm btn-success m-1">Ubah Password</a>
            </div>
          </div>
        </div>
        <div class="col-lg-12 card-body">

          <form method="POST" action="{{ route('adminupdate') }}" id="signup-form" class="signup-form">
            @csrf
            <div class="form-group">
              <label for="exampleInputEmail1">Nama</label>
              <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" id="name" placeholder="Nama" required value="{{ ($user  -> name) }}">
              @error ('name')<div class="alert alert-danger">{{ $message }}</div> @enderror
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Username</label>
              <input type="text" class="form-control @error('username') is-invalid @enderror" name="username" id="username" placeholder="Username" required value="{{ ($user  -> username) }}">
              @error ('username')<div class="alert alert-danger">{{ $message }}</div> @enderror
            </div>
            <button type="submit" class="btn btn-primary">Simpan</button>
          </form>

        </div>
      </div>
    </div>
  </div>
  <!-- Footer -->
</div>
@endsection
@section('footer')
  @include('administrator/dashboard/footer')
@endsection

@section('js')

@endsection
