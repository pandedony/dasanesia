@extends('layouts/userlayout')

@section('title', 'Dasanesia - Profile')


@section('style')

@endsection

@section('mywidget')
  @include('administrator/dashboard/widget')
@endsection

@section('topnav')
  @include('administrator/dashboard/topnav')
@endsection


@section('content')

    <!-- Header -->
    <!-- Header -->
    <div class="header bg-primary pb-9">

    </div>

<div class="container-fluid mt--8">

  <div class="row">
    <div class="col-lg-8">
      <div class="card">
        <div class="card-header border-0">
          <div class="row align-items-center">
            <div class="col">
              <h2 class="mb-0">Edit Profile</h2>
            </div>
            <div class="col text-right">
              <a href="{{route('adminedit')}}" class="btn btn-sm btn-primary m-1">Ubah Profile</a>
            </div>
          </div>
        </div>
        <div class="col-lg-12 card-body">

          <form method="POST" action="{{ route('adminpasswordupdate') }}" id="signup-form" class="signup-form">
            @csrf
            <div class="form-group">
              <label for="exampleInputEmail1">Password Lama</label>
              <input type="password" class="form-control @error('current_password') is-invalid @enderror" name="current_password" id="current_password" placeholder="Password Lama" required>
              @error ('current_password')<div class="alert alert-danger">{{ $message }}</div> @enderror
            </div>
            <div class="form-group">
              <label for="exampleInputPassword1">Password</label>
              <input type="password" class="form-control @error('password') is-invalid @enderror" name="password" id="password" placeholder="Password" required>
              @error ('password')<div class="alert alert-danger">{{ $message }}</div> @enderror
            </div>
            <div class="form-group">
              <label for="exampleInputPassword1">Ulangi Password</label>
              <input type="password" class="form-control" name="password_confirmation" id="re_password" placeholder="Repeat your password" required>
            </div>
            <button type="submit" class="btn btn-primary">Simpan</button>
          </form>

        </div>
      </div>
    </div>
  </div>
  <!-- Footer -->
</div>
@endsection

@section('footer')
  @include('administrator/dashboard/footer')
@endsection

@section('js')

@endsection
