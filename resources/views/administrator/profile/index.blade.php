@extends('layouts/userlayout')

@section('title', 'Dasanesia - Profile')


@section('style')

@endsection

@section('mywidget')
  @include('administrator/dashboard/widget')
@endsection

@section('topnav')
  @include('administrator/dashboard/topnav')
@endsection


@section('content')

    <!-- Header -->
    <!-- Header -->
    <div class="header bg-primary pb-9">

    </div>

<div class="container-fluid mt--8">

  <div class="row">
    <div class="col-lg-8">
      <div class="card">
        <div class="card-header border-0">
          <div class="row align-items-center">
            <div class="col">
              <h2 class="mb-0">Profile</h2>
            </div>

            <div class="col text-right">
              @if( Auth::user()->role  == "1")
              <a href="{{route('adminpassword')}}" class="btn btn-sm btn-success m-1">Ubah Password</a>
              <a href="{{route('adminedit')}}" class="btn btn-sm btn-primary m-1">Ubah Profile</a>
              @endif
            </div>
          </div>
        </div>
        @if (session('success'))
          <div class="alert alert-icon alert-success alert-dismissible" role="alert">
            <i class="fe fe-check mr-2" aria-hidden="true"></i>
            <button type="button" class="close" data-dismiss="alert"></button>
            {{ session('success') }}
          </div>
        @endif
        <div class="col-lg-12 card-body">

          <div class="row border-bottom py-2">
            <div class="col-lg-3">
              <b>Nama</b>
            </div>
            <div class="col-lg-9">
              {{ ($user  -> name) }}
            </div>
          </div>

          <div class="row border-bottom py-2">
            <div class="col-lg-3">
              <b>Username</b>
            </div>
            <div class="col-lg-9">
              {{ ($user  -> username) }}
            </div>
          </div>

          <div class="row border-bottom py-2">
            <div class="col-lg-3">
              <b>Role</b>
            </div>
            <div class="col-lg-9">
              @if ($user -> role == 1)
                Administrator
              @elseif ($user -> role == 2)
                Contributor
              @elseif ($user -> role == 3)
                Autor
              @endif
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>
  <!-- Footer -->
</div>
@endsection
@section('footer')
  @include('administrator/dashboard/footer')
@endsection

@section('js')

@endsection
