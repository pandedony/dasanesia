<ul class="navbar-nav">
  @if( Auth::user()->role  == "1")
  <li class="nav-item">
    <a class="nav-link" href="{{route('admindashboard')}}">
      <i class="ni ni-tv-2 text-primary"></i>
      <span class="nav-link-text">Dashboard</span>
    </a>
  </li>
  @else
  <li class="nav-item">
    <a class="nav-link" href="{{route('contributordashboard')}}">
      <i class="ni ni-tv-2 text-primary"></i>
      <span class="nav-link-text">Dashboard</span>
    </a>
  </li>
  @endif
  @if( Auth::user()->role  == "1")
  <li class="nav-item">
    <a class="nav-link" href="{{route('anggota')}}">
      <i class="ni ni-satisfied text-orange"></i>
      <span class="nav-link-text">Pengelolaan Akun</span>
    </a>
  </li>
  @endif
  <li class="nav-item">
    <a class="nav-link" href="{{route('peserta')}}">
      <i class="ni ni-circle-08 text-primary"></i>
      <span class="nav-link-text">Daftar Peserta BPJamsostek</span>
    </a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="{{route('pesertadownload')}}">
      <i class="ni ni-cloud-download-95 text-primary"></i>
      <span class="nav-link-text">Download Peserta BPJamsostek</span>
    </a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="{{route('pekerja')}}">
      <i class="ni ni-circle-08 text-success"></i>
      <span class="nav-link-text">Daftar Angkatan Kerja</span>
    </a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="{{route('pekerjadownload')}}">
      <i class="ni ni-cloud-download-95 text-success"></i>
      <span class="nav-link-text">Download Angkatan Kerja</span>
    </a>
  </li>
</ul>
