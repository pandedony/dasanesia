<footer class="footer pt-0 container-fluid">
  <div class="row align-items-center justify-content-lg-between">
    <div class="col-lg-12">
      <div class="copyright text-center  text-lg-left  text-muted">
        &copy; 2021 DASANESIA
      </div>
    </div>
  </div>
</footer>
