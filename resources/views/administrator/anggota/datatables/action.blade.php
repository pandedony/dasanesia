<form action="{{ route('anggotadestroy', $anggota->id) }}" method="post">
  {{ csrf_field() }}
  @method('delete')
  <a class="btn btn-sm btn-info" href="{{ route('anggotashow', $anggota->id) }}">Detail</a>
  <a class="btn btn-sm btn-primary" href="{{ route('anggotaedit', $anggota->id) }}">Ubah</a>
  <button class="btn btn-sm btn-danger" type="submit" onclick="return confirm('Yakin ingin menghapus data?')">Hapus</button>
</form>
