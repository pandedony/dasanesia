@extends('layouts/userlayout')

@section('title', 'Dasanesia - Pengelolaan Akun')


@section('style')

@endsection

@section('mywidget')
  @include('administrator/dashboard/widget')
@endsection

@section('topnav')
  @include('administrator/dashboard/topnav')
@endsection


@section('content')

    <!-- Header -->
    <!-- Header -->
    <div class="header bg-primary pb-9">

    </div>

<div class="container-fluid mt--8">

  <div class="row">
    <div class="col-xl-12">
      <div class="card">
        <div class="card-header border-0">
          <div class="row align-items-center">
            <div class="col">
              <h2 class="mb-0">Anggota</h2>
            </div>
            <div class="col text-right">
              <a href="{{route('anggotacreate')}}" class="btn btn-sm btn-primary">Tambah Anggota</a>
            </div>
          </div>
        </div>

        @if (session('success'))
          <div class="alert alert-icon alert-success alert-dismissible" role="alert">
            <i class="fe fe-check mr-2" aria-hidden="true"></i>
            <button type="button" class="close" data-dismiss="alert"></button>
            {{ session('success') }}
          </div>
        @endif

        <div class="col-lg-12 table-responsive">
          <!-- Projects table -->
          <table class="table align-items-center table-flush" id="datatable">
            <thead class="thead-light">
              <tr>
                <th class="w-1">No.</th>
                <th scope="col">Nama</th>
                <th scope="col">Username</th>
                <th scope="col">Role</th>
                <th scope="col">Tanggal Bergabung</th>
                <th scope="w-100"></th>
              </tr>
            </thead>
          </table>
        </div>
      </div>
    </div>
  </div>
  <!-- Footer -->
</div>
@endsection

@section('footer')
  @include('administrator/dashboard/footer')
@endsection

@section('js')
<script>
  $(function() {
      $('#datatable').DataTable({
          lengthChange: false,
          serverSide: true,
          ajax: '{{ url('anggota/get-json') }}',
          columns: [
            { data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false },
            { data: 'name', name: 'name' },
            { data: 'username', name: 'username' },
            { data: 'role', name: 'role' },
            { data: 'tanggal', name: 'tanggal' },
            { data: 'action', name: 'action', orderable: false, searchable: false },

          ],
          language: {
              "url": 'https://cdn.datatables.net/plug-ins/1.10.19/i18n/Indonesian.json'
          },
          columnDefs: [
              {
                  targets: [0],
                  className: "text-center"
              },
              {
                  targets: [2],
                  className: "text-right"
              },
          ]
      });
  });
</script>
@endsection
