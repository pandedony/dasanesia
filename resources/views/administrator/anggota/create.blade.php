@extends('layouts/userlayout')

@section('title', 'Dasanesia - Pengelolaan Akun')



@section('style')

@endsection

@section('mywidget')
  @include('administrator/dashboard/widget')
@endsection

@section('topnav')
  @include('administrator/dashboard/topnav')
@endsection


@section('content')

    <!-- Header -->
    <!-- Header -->
    <div class="header bg-primary pb-9">

    </div>

<div class="container-fluid mt--8">

  <div class="row">
    <div class="col-lg-8">
      <div class="card">
        <div class="card-header border-0">
          <div class="row align-items-center">
            <div class="col-lg-12">
              <h2 class="mb-0">Data Anggota</h2>
            </div>
          </div>
        </div>
        <div class="col-lg-12 card-body">
          <form method="POST" action="{{ route('anggotastore') }}" id="signup-form" class="signup-form">
            @csrf
            <div class="form-group">
              <label for="exampleInputEmail1">Nama</label>
              <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" id="name" placeholder="name" required>
              @error ('name')<div class="alert alert-danger">{{ $message }}</div> @enderror
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Username</label>
              <input type="text" class="form-control @error('username') is-invalid @enderror" name="username" id="username" placeholder="username" required>
              @error ('username')<div class="alert alert-danger">{{ $message }}</div> @enderror
            </div>
            <div class="form-group">
              <label for="exampleInputPassword1">Password</label>
              <input type="password" class="form-control @error('password') is-invalid @enderror" name="password" id="password" placeholder="Password" required>
              @error ('password')<div class="alert alert-danger">{{ $message }}</div> @enderror
            </div>
            <div class="form-group">
              <label for="exampleInputPassword1">Ulangi Password</label>
              <input type="password" class="form-control" name="password_confirmation" id="re_password" placeholder="Repeat your password" required>
            </div>
            <div class="form-group">
              <label for="exampleInputPassword1">Level</label>
              <select class="form-control @error('role') is-invalid @enderror" name="role" required>
                <option></option>
                <option value="2">Contributor</option>
                <option value="3">Author</option>
              </select>
              @error ('role')<div class="alert alert-danger">{{ $message }}</div> @enderror
            </div>
            <button type="submit" class="btn btn-primary">Simpan</button>
          </form>
        </div>
      </div>
    </div>
  </div>
  <!-- Footer -->

</div>
@endsection

@section('footer')
  @include('administrator/dashboard/footer')
@endsection

@section('js')

@endsection
