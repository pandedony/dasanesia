@extends('layouts/userlayout')

@section('title', 'Dasanesia - Pengelolaan Akun')



@section('style')

@endsection

@section('mywidget')
  @include('administrator/dashboard/widget')
@endsection

@section('topnav')
  @include('administrator/dashboard/topnav')
@endsection


@section('content')

    <!-- Header -->
    <!-- Header -->
    <div class="header bg-primary pb-9">

    </div>

<div class="container-fluid mt--8">

  <div class="row">
    <div class="col-lg-8">
      <div class="card">
        <div class="card-header border-0">
          <div class="row align-items-center">
            <div class="col">
              <h2 class="mb-0">Anggota</h2>
            </div>
            <div class="col text-right">
              <a href="{{ route('anggotaedit', $anggota->id) }}" class="btn btn-sm btn-primary">Ubah Anggota</a>
            </div>
          </div>
        </div>
        @if (session('success'))
          <div class="alert alert-icon alert-success alert-dismissible" role="alert">
            <i class="fe fe-check mr-2" aria-hidden="true"></i>
            <button type="button" class="close" data-dismiss="alert"></button>
            {{ session('success') }}
          </div>
        @endif
        <div class="col-lg-12 card-body">

          <div class="row border-bottom py-2">
            <div class="col-lg-3">
              <b>Nama</b>
            </div>
            <div class="col-lg-9">
              {{ ($anggota  -> name) }}
            </div>
          </div>

          <div class="row border-bottom py-2">
            <div class="col-lg-3">
              <b>Username</b>
            </div>
            <div class="col-lg-9">
              {{ ($anggota  -> username) }}
            </div>
          </div>

          <div class="row border-bottom py-2">
            <div class="col-lg-3">
              <b>Role</b>
            </div>
            <div class="col-lg-9">
              @if ($anggota -> role == 1)
                Administrator
              @elseif ($anggota -> role == 2)
                Contributor
              @elseif ($anggota -> role == 3)
                Autor
              @endif
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>
  <!-- Footer -->
</div>
@endsection

@section('footer')
  @include('administrator/dashboard/footer')
@endsection

@section('js')

@endsection
