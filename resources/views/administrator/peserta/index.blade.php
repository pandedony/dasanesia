@extends('layouts/userlayout')

@section('title', 'Dasanesia - Peserta BPJamsostek')



@section('style')

@endsection

@section('mywidget')
  @include('administrator/dashboard/widget')
@endsection

@section('topnav')
  @include('administrator/dashboard/topnav')
@endsection


@section('content')

    <!-- Header -->
    <!-- Header -->
    <div class="header bg-primary pb-9">

    </div>

<div class="container-fluid mt--8">

  <div class="row">
    <div class="col-xl-12">
      <div class="card">
        <div class="card-header border-0">
          <div class="row align-items-center">
            <div class="col">
              <h2 class="mb-0">Peserta</h2>
            </div>
            <div class="col text-right">
              @if( Auth::user()->role  == "1")
              <a href="{{route('templatepeserta')}}" class="btn btn-sm btn-info m-1">Template Excel</a>
              <button type="button" class="btn btn-success btn-sm m-1" data-toggle="modal" data-target="#importExcel">
                Import Excel
              </button>
              <a href="{{route('pesertacreate')}}" class="btn btn-sm btn-primary m-1">Tambah Peserta</a>
              <a href="{{route('pesertadeleteall')}}" class="btn btn-sm btn-danger m-1" onclick="return confirm('Yakin ingin menghapus data?')">Hapus Peserta</a>
              @elseif( Auth::user()->role  == "2")
              <a href="{{route('templatepeserta')}}" class="btn btn-sm btn-info m-1">Template Excel</a>
              <button type="button" class="btn btn-success btn-sm m-1" data-toggle="modal" data-target="#importExcel">
                Import Excel
              </button>
              <a href="{{route('pesertacreate')}}" class="btn btn-sm btn-primary m-1">Tambah Peserta</a>
              @endif
            </div>
          </div>
        </div>

        <!-- Import Excel -->
        <div class="modal fade" id="importExcel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <form method="post" action="{{route('pesertaimport')}}" enctype="multipart/form-data">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Import Excel</h5>
                </div>
                <div class="modal-body">

                  {{ csrf_field() }}

                  <label>Pilih file excel</label>
                  <div class="form-group">
                    <input type="file" name="file" required="required">
                  </div>

                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-primary">Import</button>
                </div>
              </div>
            </form>
          </div>
        </div>

        @if (session('success'))
          <div class="alert alert-icon alert-success alert-dismissible" role="alert">
            <i class="fe fe-check mr-2" aria-hidden="true"></i>
            <button type="button" class="close" data-dismiss="alert"></button>
            {{ session('success') }}
          </div>
        @endif

        {{-- notifikasi form validasi --}}
        @if ($errors->has('file'))
        <span class="invalid-feedback" role="alert">
          <strong>{{ $errors->first('file') }}</strong>
        </span>
        @endif

        {{-- notifikasi sukses --}}
        @if ($sukses = Session::get('sukses'))
        <div class="alert alert-success alert-block">
          <button type="button" class="close" data-dismiss="alert">×</button>
          <strong>{{ $sukses }}</strong>
        </div>
        @endif

        <div class="col-lg-12 table-responsive">

          <!-- Projects table -->
          <table class="table" id="datatable" style="width:100%">
            <thead class="thead-light">
              <tr>
                <th class="w-1">No.</th>
                <th scope="col">Nik</th>
                <th scope="col">Nama</th>
                <th scope="col">Kecamatan</th>
                <th scope="col">Tanggal Lahir</th>
                <th scope="col">Program</th>
                <th scope="col">Tanggal Grace</th>
                <th scope="col">Kanal Daftar</th>
                <th scope="col">Kanal Bayar</th>
                <th scope="col">Nomor Hp</th>
                <th scope="w-100"></th>
              </tr>
            </thead>
          </table>

        </div>
      </div>
    </div>
  </div>
  <!-- Footer -->
</div>
@endsection

@section('footer')
  @include('administrator/dashboard/footer')
@endsection

@section('js')
<script>
  $(function() {
      $('#datatable').DataTable({
          processing: true,
          serverSide: true,
          ajax: '{{ url('peserta/get-json') }}',
          columns: [
            { data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false },
            { data: 'nik', name: 'nik' },
            { data: 'nama', name: 'nama' },
            { data: 'kecamatan', name: 'kecamatan' },
            { data: 'tanggallahir', name: 'tanggallahir' },
            { data: 'program', name: 'program' },
            { data: 'tanggalgrace', name: 'tanggalgrace' },
            { data: 'kanal_daftar', name: 'kanal_daftar' },
            { data: 'kanal_bayar', name: 'kanal_daftar' },
            { data: 'nomor_hp', name: 'nomor_hp' },
            { data: 'action', name: 'action', orderable: false, searchable: false },

          ],
          language: {
              "url": 'https://cdn.datatables.net/plug-ins/1.10.19/i18n/Indonesian.json'
          },
          columnDefs: [
              {
                  targets: [0],
                  className: "text-center"
              },
              {
                  targets: [2],
                  className: "text-right"
              },
          ]
      });
  });
</script>
@endsection
