@if( Auth::user()->role  == "1")
<form action="{{ route('pesertadestroy', $peserta->nik) }}" method="post">
  {{ csrf_field() }}
  @method('delete')
  <a class="btn btn-sm btn-info" href="{{ route('pesertashow', $peserta->nik) }}">Detail</a></br>
  <a class="btn btn-sm btn-primary" href="{{ route('pesertaedit', $peserta->nik) }}">Ubah</a></br>
  <button class="btn btn-sm btn-danger" type="submit" onclick="return confirm('Yakin ingin menghapus data?')">Hapus</button>
</form>
@else
<a class="btn btn-sm btn-info" href="{{ route('pesertashow', $peserta->nik) }}">Detail</a></br>
@endif
