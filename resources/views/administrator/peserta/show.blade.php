@extends('layouts/userlayout')

@section('title', 'Dasanesia - Peserta BPJamsostek')


@section('style')

@endsection

@section('mywidget')
  @include('administrator/dashboard/widget')
@endsection

@section('topnav')
  @include('administrator/dashboard/topnav')
@endsection


@section('content')

    <!-- Header -->
    <!-- Header -->
    <div class="header bg-primary pb-9">

    </div>

<div class="container-fluid mt--8">

  <div class="row">
    <div class="col-lg-8">
      <div class="card">
        <div class="card-header border-0">
          <div class="row align-items-center">
            <div class="col">
              <h2 class="mb-0">Peserta</h2>
            </div>
            <div class="col text-right">
              @if( Auth::user()->role  == "1")
              <a href="{{ route('pesertaedit', $peserta->nik) }}" class="btn btn-sm btn-primary">Ubah Peserta</a>
              @endif
            </div>
          </div>
        </div>
        @if (session('success'))
          <div class="alert alert-icon alert-success alert-dismissible" role="alert">
            <i class="fe fe-check mr-2" aria-hidden="true"></i>
            <button type="button" class="close" data-dismiss="alert"></button>
            {{ session('success') }}
          </div>
        @endif
        <div class="col-lg-12 card-body">

          <div class="row border-bottom py-2">
            <div class="col-lg-3">
              <b>NIK</b>
            </div>
            <div class="col-lg-9">
              {{ ($peserta  -> nik) }}
            </div>
          </div>

          <div class="row border-bottom py-2">
            <div class="col-lg-3">
              <b>Nama</b>
            </div>
            <div class="col-lg-9">
              {{ ($peserta  -> nama) }}
            </div>
          </div>

          <div class="row border-bottom py-2">
            <div class="col-lg-3">
              <b>Kecamatan</b>
            </div>
            <div class="col-lg-9">
              {{ ($peserta  -> kecamatan) }}
            </div>
          </div>

          <div class="row border-bottom py-2">
            <div class="col-lg-3">
              <b>Tanggal Lahir</b>
            </div>
            <div class="col-lg-9">
              {{ ($peserta  -> tgl_lahir->format('d F Y')) }}
            </div>
          </div>

          <div class="row border-bottom py-2">
            <div class="col-lg-3">
              <b>Program</b>
            </div>
            <div class="col-lg-9">
              {{ ($peserta  -> program) }}
            </div>
          </div>

          <div class="row border-bottom py-2">
            <div class="col-lg-3">
              <b>Tanggal Grace</b>
            </div>
            <div class="col-lg-9">
              {{ ($peserta  -> tgl_grace->format('d F Y')) }}
            </div>
          </div>

          <div class="row border-bottom py-2">
            <div class="col-lg-3">
              <b>Kanal Daftar</b>
            </div>
            <div class="col-lg-9">
              {{ ($peserta  -> kanal_daftar) }}
            </div>
          </div>

          <div class="row border-bottom py-2">
            <div class="col-lg-3">
              <b>Kanal Bayar</b>
            </div>
            <div class="col-lg-9">
              {{ ($peserta  -> kanal_bayar) }}
            </div>
          </div>

          <div class="row border-bottom py-2">
            <div class="col-lg-3">
              <b>Nomor Hp</b>
            </div>
            <div class="col-lg-9">
              {{ ($peserta  -> nomor_hp) }}
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>
  <!-- Footer -->
</div>
@endsection

@section('footer')
  @include('administrator/dashboard/footer')
@endsection

@section('js')

@endsection
