@extends('layouts/userlayout')

@section('title', 'Dasanesia - Peserta BPJamsostek')


@section('style')

@endsection

@section('mywidget')
  @include('administrator/dashboard/widget')
@endsection

@section('topnav')
  @include('administrator/dashboard/topnav')
@endsection


@section('content')

    <!-- Header -->
    <!-- Header -->
    <div class="header bg-primary pb-9">

    </div>

<div class="container-fluid mt--8">

  <div class="row">
    <div class="col-lg-8">
      <div class="card">
        <div class="card-header border-0">
          <div class="row align-items-center">
            <div class="col">
              <h2 class="mb-0">Edit Peserta</h2>
            </div>
          </div>
        </div>
        <div class="col-lg-12 card-body">

          <form method="POST" action="{{ route('pesertaupdate', $peserta->nik) }}" id="signup-form" class="signup-form">
            @csrf
            <div class="row">
              <div class="col-md-6 my-2">
                NIK
                <input type="number" class="form-control @error('name') is-invalid @enderror" name="nik" id="nik" placeholder="NIK" value="{{ $peserta->nik}}" required>
                @error ('nik')<div class="alert alert-danger">{{ $message }}</div> @enderror
              </div>
              <div class="col-md-6 my-2">
                Nama
                <input type="text" class="form-control @error('nama') is-invalid @enderror" name="nama" id="nama" placeholder="Nama" value="{{ $peserta -> nama }}" required>
                @error ('nama')<div class="alert alert-danger">{{ $message }}</div> @enderror
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 my-2">
                Tanggal Lahir
                <input type="date" class="form-control" name="tgl_lahir" value="{{ $peserta  -> tgl_lahir->format('Y-m-d') }}">
              </div>
              <div class="col-md-6 my-2">
                Kecamatan
                <select class="form-control" name="kecamatan">
                  <option value="{{$peserta->wilayah_id}}">{{ $peserta->kecamatan }}</option>
                  @foreach ($wilayah as $w)
                    <option value="{{$w->kode_wilayah}}">{{$w->nama_wilayah}}</option>
                  @endforeach
                </select>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 my-2">
                Program
                <input type="text" class="form-control @error('program') is-invalid @enderror" name="program" id="program" placeholder="Program" value="{{ $peserta -> program }}" required>
                @error ('program')<div class="alert alert-danger">{{ $message }}</div> @enderror
              </div>
              <div class="col-md-6 my-2">
                Tanggal Grace
                <input type="date" class="form-control" name="tgl_grace" value="{{ $peserta  -> tgl_grace->format('Y-m-d') }}">
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 my-2">
                Kanal Daftar
                <input type="text" class="form-control @error('kanal_daftar') is-invalid @enderror" name="kanal_daftar" id="kanal_daftar" placeholder="Kanal Daftar" value="{{ $peserta->kanal_daftar }}" required>
                @error ('kanal_daftar')<div class="alert alert-danger">{{ $message }}</div> @enderror
              </div>
              <div class="col-md-6 my-2">
                Kanal Bayar
                <input type="text" class="form-control @error('kanal_bayar') is-invalid @enderror" name="kanal_bayar" id="kanal_bayar" placeholder="Kanal Bayar" value="{{ $peserta-> kanal_bayar }}" required>
                @error ('kanal_bayar')<div class="alert alert-danger">{{ $message }}</div> @enderror
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 my-2">
                Nomor HP
                <input type="number" class="form-control @error('no_hp') is-invalid @enderror" name="no_hp" id="no_hp" placeholder="Nomor HP" value="{{ $peserta -> nomor_hp }}" required>
                @error ('no_hp')<div class="alert alert-danger">{{ $message }}</div> @enderror
              </div>
              <div class="col-md-6 my-2 pt-4">
                <button type="submit" class="btn btn-primary">Simpan</button>
              </div>
            </div>
          </form>

        </div>
      </div>
    </div>
  </div>
  <!-- Footer -->
</div>
@endsection

@section('footer')
  @include('administrator/dashboard/footer')
@endsection

@section('js')

@endsection
