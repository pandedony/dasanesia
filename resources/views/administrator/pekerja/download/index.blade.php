@extends('layouts/userlayout')

@section('title', 'Dasanesia - Angkatan Kerja')


@section('style')

@endsection

@section('mywidget')
  @include('administrator/dashboard/widget')
@endsection

@section('topnav')
  @include('administrator/dashboard/topnav')
@endsection


@section('content')

    <!-- Header -->
    <!-- Header -->
    <div class="header bg-primary pb-9">

    </div>

<div class="container-fluid mt--8">

  <div class="row">
    <div class="col-xl-12">
      <div class="card">
        <div class="card-header border-0">
          <div class="row align-items-center">
            <div class="col">
              <h2 class="mb-0">Peserta</h2>
            </div>
          </div>
        </div>


        @if (session('success'))
          <div class="alert alert-icon alert-success alert-dismissible" role="alert">
            <i class="fe fe-check mr-2" aria-hidden="true"></i>
            <button type="button" class="close" data-dismiss="alert"></button>
            {{ session('success') }}
          </div>
        @endif

        {{-- notifikasi form validasi --}}
        @if ($errors->has('file'))
        <span class="invalid-feedback" role="alert">
          <strong>{{ $errors->first('file') }}</strong>
        </span>
        @endif

        {{-- notifikasi sukses --}}
        @if ($sukses = Session::get('sukses'))
        <div class="alert alert-success alert-block">
          <button type="button" class="close" data-dismiss="alert">×</button>
          <strong>{{ $sukses }}</strong>
        </div>
        @endif

        <div class="col-lg-12 table-responsive">

          <!-- Projects table -->
          <table class="table" id="datatable" style="width:100%">
            <thead class="thead-light">
              <tr>
                <th class="w-1">No.</th>
                <th scope="col">Nama Wilayah</th>
                <th scope="col">Total Peserta</th>
                <th scope="w-100"></th>
              </tr>
            </thead>
          </table>

        </div>
      </div>
    </div>
  </div>
  <!-- Footer -->

</div>
@endsection

@section('footer')
  @include('administrator/dashboard/footer')
@endsection

@section('js')
<script>
  $(function() {
      $('#datatable').DataTable({
          processing: true,
          serverSide: true,
          ajax: '{{ url('pekerja/download/get-json') }}',
          columns: [
            { data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false },
            { data: 'nama_wilayah', name: 'nama_wilayah' },
            { data: 'jumlah', name: 'jumlah' },
            { data: 'action', name: 'action', orderable: false, searchable: false },

          ],
          language: {
              "url": 'https://cdn.datatables.net/plug-ins/1.10.19/i18n/Indonesian.json'
          },
          columnDefs: [
              {
                  targets: [0],
                  className: "text-center"
              },
              {
                  targets: [2],
                  className: "text-center"
              },
          ]
      });
  });
</script>
@endsection
