@extends('layouts/userlayout')

@section('title', 'Dasanesia - Angkatan Kerja')


@section('style')

@endsection

@section('mywidget')
  @include('administrator/dashboard/widget')
@endsection

@section('topnav')
  @include('administrator/dashboard/topnav')
@endsection


@section('content')

    <!-- Header -->
    <!-- Header -->
    <div class="header bg-primary pb-9">

    </div>

<div class="container-fluid mt--8">

  <div class="row">
    <div class="col-lg-12">
      <div class="card">
        <div class="card-header border-0">
          <div class="row align-items-center">
            <div class="col-lg-12">
              <h2 class="mb-0">Data Anggota</h2>
            </div>
          </div>
        </div>
        <div class="col-lg-12 card-body">
          <form method="POST" action="{{ route('pekerjastore') }}" id="signup-form" class="signup-form">
            @csrf
            <div class="row">
              <div class="col-md-6 my-2">
                NIK
                <input type="number" class="form-control @error('name') is-invalid @enderror" name="nik" id="nik" placeholder="NIK" value="{{ old('nik') }}" required>
                @error ('nik')<div class="alert alert-danger">{{ $message }}</div> @enderror
              </div>
              <div class="col-md-6 my-2">
                Nama
                <input type="text" class="form-control @error('nama') is-invalid @enderror" name="nama" id="nama" placeholder="Nama" value="{{ old('nama') }}" required>
                @error ('nama')<div class="alert alert-danger">{{ $message }}</div> @enderror
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 my-2">
                Jenis Kelamin
                <select  class="form-control" name="jk" required>
                  <option>Jenis Kelamin</option>
                  <option value="LAKI-LAKI">Laki Laki</option>
                  <option value="PEREMPUAN">Perempuan</option>
                </select>
              </div>
              <div class="col-md-6 my-2">
                Tanggal Lahir
                <input type="date" class="form-control" name="tgl_lahir" value="{{ old('tgl_lahir') }}">
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 my-2">
                Pendidikan Terakhir
                <input type="text" class="form-control @error('pendidikan') is-invalid @enderror" name="pendidikan" id="pendidikan" placeholder="Pendidikan" value="{{ old('pendidikan') }}">
                @error ('pendidikan')<div class="alert alert-danger">{{ $message }}</div> @enderror
              </div>
              <div class="col-md-6 my-2">
                Pekerjaan
                <input type="text" class="form-control @error('pekerjaan') is-invalid @enderror" name="pekerjaan" id="pekerjaan" placeholder="Pekerjaan" value="{{ old('pekerjaan') }}" required>
                @error ('pekerjaan')<div class="alert alert-danger">{{ $message }}</div> @enderror
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 my-2">
                Kelurahan
                <input type="text" class="form-control @error('kelurahan') is-invalid @enderror" name="kelurahan" id="kelurahan" placeholder="Kelurahan" value="{{ old('kelurahan') }}">
                @error ('kelurahan')<div class="alert alert-danger">{{ $message }}</div> @enderror
              </div>
              <div class="col-md-6 my-2">
                <div class="row">
                  <div class="col">
                    Rt
                    <input type="number" class="form-control @error('rt') is-invalid @enderror" name="rt" id="rt" placeholder="RT" value="{{ old('rt') }}">
                  </div>
                  <div class="col">
                    Rw
                    <input type="number" class="form-control @error('rw') is-invalid @enderror" name="rw" id="rw" placeholder="RW" value="{{ old('rw') }}">
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 my-2">
                Kecamatan
                <select class="form-control" name="kecamatan" required>
                  <option>Pilih Kecamatan</option>
                  @foreach ($wilayah as $w)
                    <option value="{{$w->kode_wilayah}}">{{$w->nama_wilayah}}</option>
                  @endforeach
                </select>
              </div>
              <div class="col-md-6 my-2">
                Kabupaten
                <input type="text" class="form-control @error('kabupaten') is-invalid @enderror" name="kabupaten" id="kabupaten" value="TIMOR TENGAH UTARA" disabled>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 my-2 pt-4">
                <button type="submit" class="btn btn-primary">Simpan</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  <!-- Footer -->
</div>
@endsection

@section('footer')
  @include('administrator/dashboard/footer')
@endsection

@section('js')

@endsection
