@extends('layouts/userlayout')

@section('title', 'Dasanesia - Angkatan Kerja')


@section('style')

@endsection

@section('mywidget')
  @include('administrator/dashboard/widget')
@endsection

@section('topnav')
  @include('administrator/dashboard/topnav')
@endsection


@section('content')

    <!-- Header -->
    <!-- Header -->
    <div class="header bg-primary pb-9">

    </div>

<div class="container-fluid mt--8">

  <div class="row">
    <div class="col-xl-12">
      <div class="card">
        <div class="card-header border-0">
          <div class="row align-items-center">
            <div class="col">
              <h2 class="mb-0">Peserta</h2>
            </div>
            <div class="col text-right">
              @if( Auth::user()->role  == "1")
              <a href="{{route('templatepekerja')}}" class="btn btn-sm btn-info m-1">Template Excel</a>
              <button type="button" class="btn btn-success btn-sm m-1" data-toggle="modal" data-target="#importExcel">
                Import Excel
              </button>
              <a href="{{route('pekerjacreate')}}" class="btn btn-sm btn-primary m-1">Tambah Pekerja</a>
              <a href="{{route('pekerjadestroyall')}}" class="btn btn-sm btn-danger m-1" onclick="return confirm('Yakin ingin menghapus data?')">Hapus Pekerja</a>
              @endif
            </div>
          </div>
        </div>

        <!-- Import Excel -->
        @if( Auth::user()->role  == "1")
        <div class="modal fade" id="importExcel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <form method="post" action="{{route('pekerjaimport')}}" enctype="multipart/form-data">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Import Data Pekerja</h5>
                </div>
                <div class="modal-body">

                  {{ csrf_field() }}

                  <label>Pilih file excel</label>
                  <div class="form-group">
                    <input type="file" name="file" required="required">
                  </div>

                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-primary">Import</button>
                </div>
              </div>
            </form>
          </div>
        </div>
        @endif

        @if (session('success'))
          <div class="alert alert-icon alert-success alert-dismissible" role="alert">
            <i class="fe fe-check mr-2" aria-hidden="true"></i>
            <button type="button" class="close" data-dismiss="alert"></button>
            {{ session('success') }}
          </div>
        @endif

        {{-- notifikasi form validasi --}}
        @if ($errors->has('file'))
        <span class="invalid-feedback" role="alert">
          <strong>{{ $errors->first('file') }}</strong>
        </span>
        @endif

        {{-- notifikasi sukses --}}
        @if ($sukses = Session::get('sukses'))
        <div class="alert alert-success alert-block">
          <button type="button" class="close" data-dismiss="alert">×</button>
          <strong>{{ $sukses }}</strong>
        </div>
        @endif

        <div class="col-lg-12 table-responsive">

          <!-- Projects table -->
          <table class="table" id="datatable" style="width:100%; word-break: break-all;">
            <thead class="thead-light">
              <tr>
                <th class="w-1">No.</th>
                <th scope="col">Nik</th>
                <th scope="col">Nama</th>
                <th scope="col">Jenis Kelamin</th>
                <th scope="col">Tanggal Lahir</th>
                <th scope="col">Pendidikan</th>
                <th scope="col">Pekerjaan</th>
                <th scope="col">Alamat</th>
                <th scope="col">Kabupaten</th>
                <th scope="w-100"></th>
              </tr>
            </thead>
          </table>

        </div>
      </div>
    </div>
  </div>
  <!-- Footer -->
</div>
@endsection

@section('footer')
  @include('administrator/dashboard/footer')
@endsection

@section('js')
<script>
  $(function() {
      $('#datatable').DataTable({
          processing: true,
          serverSide: true,
          ajax: '{{ url('pekerja/get-json') }}',
          columns: [
            { data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false },
            { data: 'nik', name: 'nik' },
            { data: 'nama', name: 'nama' },
            { data: 'jk', name: 'jk' },
            { data: 'tanggallahir', name: 'tanggallahir' },
            { data: 'pendidikan', name: 'pendidikan' },
            { data: 'pekerjaan', name: 'pekerjaan' },
            { data: 'alamat', name: 'alamat', width: "50px" },
            { data: 'kabupaten', name: 'kabupaten' },
            { data: 'action', name: 'action', orderable: false, searchable: false },

          ],
          language: {
              "url": 'https://cdn.datatables.net/plug-ins/1.10.19/i18n/Indonesian.json'
          },
          columnDefs: [
              {
                  targets: [0],
                  className: "text-center"
              },
              {
                  targets: [2],
                  className: "text-right"
              },
              {
                  targets: [7],
                  className: "tableasd"
              },
          ]
      });
  });
</script>
@endsection
