@extends('layouts/userlayout')

@section('title', 'Dasanesia - Dashboard')


@section('style')

@endsection

@section('mywidget')
  @include('administrator/dashboard/widget')
@endsection

@section('topnav')
  @include('administrator/dashboard/topnav')
@endsection


@section('content')

    <!-- Header -->
    <!-- Header -->
    <div class="header bg-primary pb-6">
      <div class="container-fluid pt-4">
        <div class="header-body">
          <!-- Card stats -->
          <div class="row">

            <div class="col-md-4">
              <div class="card card-stats">
                <!-- Card body -->
                <div class="card-body">
                  <div class="row">
                    <div class="col">
                      <h5 class="card-title text-uppercase text-muted mb-0">Total Pekerja</h5>
                      <span class="h2 font-weight-bold mb-0">{{$totalpekerja}}</span>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape bg-gradient-blue text-white rounded-circle shadow">
                        <i class="ni ni-chart-bar-32"></i>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="col-md-4">
              <div class="card card-stats">
                <!-- Card body -->
                <div class="card-body">
                  <div class="row">
                    <div class="col">
                      <h5 class="card-title text-uppercase text-muted mb-0">Total Peserta Jamsostek</h5>
                      <span class="h2 font-weight-bold mb-0">{{$totalpeserta}}</span>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape bg-gradient-blue text-white rounded-circle shadow">
                        <i class="ni ni-chart-bar-32"></i>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="col-md-4">
              <div class="card card-stats">
                <!-- Card body -->
                <div class="card-body">
                  <div class="row">
                    <div class="col">
                      <h5 class="card-title text-uppercase text-muted mb-0">Selisih</h5>
                      <span class="h2 font-weight-bold mb-0">{{$totalpekerja - $totalpeserta}}</span>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape bg-gradient-blue text-white rounded-circle shadow">
                        <i class="ni ni-chart-pie-35"></i>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

          </div>
        </div>
      </div>
    </div>

<div class="container-fluid mt--6">

  <div class="row">
    <div class="col-xl-8">
      <div class="card">
        <div class="card-header border-0">
          <div class="row align-items-center">
            <div class="col">
              <h3 class="mb-0">Pekerja Baru</h3>
            </div>
            <div class="col text-right">
              <a href="{{route('pekerja')}}" class="btn btn-sm btn-primary">See all</a>
            </div>
          </div>
        </div>
        <div class="table-responsive">
          <!-- Projects table -->
          <table class="table align-items-center table-flush">
            <thead class="thead-light">
              <tr>
                <th scope="col">Page name</th>
                <th scope="col">Visitors</th>
                <th scope="col">Unique users</th>
                <th scope="col">Bounce rate</th>
              </tr>
            </thead>
            <tbody>
              @foreach ($pekerja as $p)
                <tr>
                  <td>{{$p -> nik}}</td>
                  <td>{{$p -> nama}}</td>
                  <td>{{$p -> jk}}</td>
                  <td>{{$p -> created_at ->diffForHumans()}}</td>
                </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <div class="col-xl-4">
      <div class="card">
        <div class="card-header border-0">
          <div class="row align-items-center">
            <div class="col">
              <h3 class="mb-0">Grafik</h3>
            </div>
          </div>
        </div>
        <div class="card-body" >
          <canvas width="500vh" id="myChart"></canvas>
        </div>
        <div class="row">
          <div class="col m-1">
            <i class="ni ni-chart-pie-35 text-red"></i>
            Peserta Jamsostek
          </div>
          <div class="col m-1">
            <i class="ni ni-chart-pie-35 text-blue"></i>
            Bukan Peserta Jamsostek
          </div>
        </div>
      </div>
    </div>


  </div>
  <!-- Footer -->
</div>
@endsection

@section('footer')
  @include('administrator/dashboard/footer')
@endsection

@section('js')
<script>
  var ctx = document.getElementById('myChart').getContext('2d');
  var myChart = new Chart(ctx, {
      type: 'pie',
      data: {
          labels: ['Peserta Jamsostek', 'Bukan Peserta Jamsostek'],
          responsive: true,
          plugins: {
            legend: {
              position: 'top',
            },
            title: {
              display: true,
              text: 'Chart.js Pie Chart'
            }
          },
          datasets: [{
              label: 'Pande Dony',
              data: [
                {{ $totalpeserta }},
                {{$totalpekerja - $totalpeserta }},
              ],
              backgroundColor: [
                  'rgba(255, 99, 132)',
                  'rgba(54, 162, 235)',
              ],
          }]
      },
      options: {
          scales: {
              y: {
                  beginAtZero: true
              }
          }
      }
  });
  </script>
@endsection
