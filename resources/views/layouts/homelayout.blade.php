<!DOCTYPE html>

<html lang="en">
<head>
  <!-- Meta Tags -->
  <meta charset="utf-8"/>

  <!-- Site Title-->
  <title>Dasanesia | Dari Desa Untuk Indonesia</title>

  <link href='{{asset('assets/icon.ico')}}' rel='shortcut icon'>

  <!-- Mobile Specific Metas-->
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>

  <!-- Google-fonts -->
  <link href='http://fonts.googleapis.com/css?family=Signika+Negative:300,400,600,700' rel='stylesheet' type='text/css'>
  <link href='http://fonts.googleapis.com/css?family=Kameron:400,700' rel='stylesheet' type='text/css'>

  <!-- Bootstrap -->

  <link rel="stylesheet" href="{{asset('assets/vendor/delex/css/bootstrap.min.css')}}"/>
  <!-- Fonts-style -->
  <link rel="stylesheet" href="{{asset('assets/vendor/delex/css/styles.css')}}"/>
  <!-- Fonts-style -->
  <link rel="stylesheet" href="{{asset('assets/vendor/delex/css/font-awesome.min.css')}}"/>
  <!-- Modal-Effect -->
  <link href="{{asset('assets/vendor/delex/css/component.css')}}" rel="stylesheet">
  <!-- owl-carousel -->
  <link href="{{asset('assets/vendor/delex/css/owl.carousel.css')}}" rel="stylesheet" type="text/css" media="screen">
  <link href="{{asset('assets/vendor/delex/css/owl.theme.css')}}" rel="stylesheet" type="text/css" media="screen">
  <!-- Template Styles-->
  <link rel="stylesheet" href="{{asset('assets/vendor/delex/css/style.css')}}"/>
  <!-- Template Color-->
  <link rel="stylesheet" type="text/css" href="{{asset('assets/vendor/delex/css/green.css')}}" media="screen" id="color-opt" />



</head>

<body data-spy="scroll" data-offset="80">

  <!-- Preloader -->
  <div class="animationload">
    <div class="loader">
        Please Wait....
    </div>
  </div>
  <!-- End Preloader -->


  <nav class="navbar navbar-default navbar-fixed-top navbar-custom">
    <div class="container">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="p-1 navbar-brand-custom" href="#">
          <img src="{{asset('assets/vendor/delex/images/logobpjs.png')}}"alt="">
        </a>
      </div>

      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav navbar-right">
          <li><a href="{{route('home')}}">Home</a></li>
          <li><a href="{{route('datapekerja')}}">Pendataan Pekerja</a></li>
          <li><a href="{{route('login')}}">Login</a></li>
        </ul>
      </div><!-- /.navbar-collapse -->
    </div><!-- /.container -->
  </nav>

    <!-- /HOME -->
    <section class="main-home" id="home">
      <div class="home-page-photo"></div> <!-- Background image -->
      <div class="home__header-content">
        <div id="main-home-carousel" class="owl-carousel">
          <div>
            <h4 class="intro-title">selamat datang di</h4>
            <h1 class="intro-title" style="font-size: 100px">D A S A N E S I A</h1>
            <h3 class="intro-title">dari desa untuk indonesia</h3>
          </div><!--slide item like paragraph-->

        </div>
      </div>
    </section>
    <!-- /End HOME -->

    <!-- / SERVICES -->
    @yield('content')

    <!-- FOOTER begings -->
    <footer id="footer">
      <div class="footer-widgets-wrap">
        <div class="container text-center">
          <div class="row">
            <div class="col-sm-12 col-md-12">
              <div class="footer-content">
                <h4 class="whitecolor">KEEP IN TOUCH</h4>
                <div class="footer-socials">
                  <a href="https://www.instagram.com/sahabat_pekerja_ttu/" target="_blank"><i class="whitecolor fa fa-instagram text-white"></i></a>
                </div>
              </div> <!-- end footer-content -->
            </div> <!-- end col-sm-4 -->
          </div> <!-- end row -->
        </div> <!-- container -->
      </div>
      <div class="footer-bottom text-center whitecolor"> <!-- Footer-copyright -->
        <p class="whitecolor">©2021 DASANESIA</p>
      </div>
    </footer>
    <!-- End footer begings -->


    <!-- Scroll top -->
    <a href="#" class="back-to-top"> <i class="fa fa-chevron-up"> </i> </a>



    <!-- JavaScript
     ================================================== -->
     <!-- Placed at the end of the document so the pages load faster -->
     <!-- initialize jQuery Library -->
     <script src="{{asset('assets/vendor/delex/js/jquery.min.js')}}"></script>
     <!-- jquery easing -->
     <script src="{{asset('assets/vendor/delex/js/jquery.easing.min.js')}}"></script>
     <!-- Bootstrap -->
     <script src="{{asset('assets/vendor/delex/js/bootstrap.min.js')}}"></script>
     <!-- modal-effect -->
     <script src="{{asset('assets/vendor/delex/js/classie.js')}}"></script>
     <script src="{{asset('assets/vendor/delex/js/modalEffects.js')}}"></script>
     <!-- Counter-up -->
     <script src="{{asset('assets/vendor/delex/js/waypoints.min.js')}}" type="text/javascript"></script>
     <script src="{{asset('assets/vendor/delex/js/jquery.counterup.min.js')}}" type="text/javascript"></script>
     <!-- SmoothScroll -->
     {{-- <script src="{{asset('assets/vendor/delex/js/SmoothScroll.js')}}"></script> --}}
     <!-- Parallax -->
     <script src="{{asset('assets/vendor/delex/js/jquery.stellar.min.js')}}"></script>
     <!-- Jquery-Nav -->
     <script src="{{asset('assets/vendor/delex/js/jquery.nav.js')}}"></script>
     <!-- Owl carousel -->
     <script type="text/javascript" src="{{asset('assets/vendor/delex/js/owl.carousel.min.js')}}"></script>
     <!-- App JS -->
     <script src="{{asset('assets/vendor/delex/js/app.js')}}"></script>

     <!-- Switcher script for demo only -->
    <script type="text/javascript" src="{{asset('assets/vendor/delex/js/switcher.js')}}"></script>


  </body>
</html>
