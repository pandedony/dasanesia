@extends('layouts/homelayout')
@section('content')
<section id="services">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h3 class="title text-center">Data Kepesertaan</h3>
        <div class="titleHR"><span></span></div>
      </div>
    </div>
    <div class="row">
      @foreach ($wilayah as $w)
      <div class="col-lg-4">
        <div class="table-responsive">
          <!-- Projects table -->
          <table class="table align-items-center table-flush">
            <thead class="thead-light">
              <tr>
                <th scope="col" colspan="3">{{ $w -> nama_wilayah}}</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <th scope="row">
                  Angkatan Kerja
                </th>
                <td colspan="2" width="50%">
                  <div class="d-flex align-items-center">
                    {{-- {{($w->total_pekerja / $w->total_pekerja * 100) . '%'}} --}}
                    <span class="mr-2">{{$w->total_pekerja}}</span>
                    <div>
                      <div class="progress">
                        <div class="progress-bar bg-light-blue" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 90%;"></div>
                      </div>
                    </div>
                  </div>
                </td>
              </tr>
              <tr>
                <th scope="row">
                  Peserta BPJamsostek
                </th>
                <td colspan="2">
                  <div class="d-flex align-items-center">
                    <span class="mr-2">{{$w->total_peserta}}</span>
                    <div>
                      <div class="progress">
                        <div class="progress-bar bg-light-green" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width: 60%;"></div>
                      </div>
                    </div>
                  </div>
                </td>
              </tr>
              <tr>
                <th scope="row">
                  Belum Peserta BPJamsostek
                </th>
                <td colspan="2">
                  <div class="d-flex align-items-center">
                    <span class="mr-2">{{$w->total_pekerja - $w->total_peserta}}</span>
                    <div>
                      <div class="progress">
                        <div class="progress-bar bg-light-yellow" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 70%;"></div>
                      </div>
                    </div>
                  </div>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
        <h5>
      </div>
      @endforeach

    </div> <!--/.row -->

  </div> <!--/.container -->
</section>
<!-- / End services-->


@endsection
